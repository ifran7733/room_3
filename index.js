require('dotenv').config(); //untuk membaca file .env
const express = require('express');
const port = process.env.PORT || 3500;
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const router = require('./router'); // menambahkan variabel router
const YAML = require('yamljs');
const swaggerUI = require('swagger-ui-express'); //inisiasi variabel yang berisi swagger ui
const apiDocs = YAML.load('./api-docs.yaml'); //inisiasi variabel yang berisi file api-doc.yaml

app.use(cors()); // gunakan fungsi cors
app.use(express.urlencoded({ extended: true })); //untuk menerima request formdata dan urlencode form
app.use(bodyParser.json()); //untuk menangkap json request
app.use('/api', router); // gunakan fungsi router
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(apiDocs)); // impplementasikan router swagger

app.listen(port, () => {
  console.log(`Server is running at port ${port}`);
});
