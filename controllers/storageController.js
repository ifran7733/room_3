const model = require('../models');

module.exports = {
  // untuk menampilkan semua data di dalam tabel storage
  list: async (req, res) => {
    try {
      const datas = await model.storage.findAll();

      return res.status(200).json({
        success: true,
        error: 0,
        message: 'data success listed',
        data: datas,
      });
    } catch (error) {
      return res.status(500).json({
        success: false,
        error: error.code,
        message: error,
        data: null,
      });
    }
  },
  // untuk menambahkan data baru di dalam tabel storage
  create: async (req, res) => {
    try {
      const data = await model.storage.create(req.body);

      return res.status(200).json({
        success: true,
        error: 0,
        message: 'data success create',
        data: data,
      });
    } catch (error) {
      return res.status(500).json({
        success: false,
        error: error.code,
        message: error,
        data: null,
      });
    }
  },
  // untuk update data di dalam tabel storage
  update: async (req, res) => {
    try {
      const data = await model.storage.update(
        {
          name: req.body.name,
          description: req.body.description,
          userId: req.body.userId,
        },
        {
          where: {
            id: req.params.id,
          },
        }
      );

      return res.status(200).json({
        success: true,
        error: 0,
        message: 'data success update',
        data: data,
      });
    } catch (error) {
      return res.status(500).json({
        success: false,
        error: error.code,
        message: error,
        data: null,
      });
    }
  },
  // untuk menghapus data di dalam tabel storage
  destroy: async (req, res) => {
    try {
      const data = await model.storage.destroy({
        where: {
          id: req.params.id,
        },
      });

      return res.status(200).json({
        success: true,
        error: 0,
        message: 'data success delete',
        data: data,
      });
    } catch (error) {
      return res.status(500).json({
        success: false,
        error: error.code,
        message: error,
        data: null,
      });
    }
  },
};
