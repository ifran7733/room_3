const model = require('../models');
module.exports = {
  list: async (req, res) => {
    try {
      const datas = await model.user.findAll();

      return res.status(200).json({
        success: true,
        error: 0,
        message: 'data successfully listed',
        data: datas,
      });
    } catch (error) {
      return res.status(500).json({
        success: false,
        error: error.code,
        message: error,
        data: null,
      });
    }
  },
  create: async (req, res) => {
    try {
      const data = await model.user.create(req.body);

      return res.status(200).json({
        success: true,
        error: 0,
        message: 'data successfully listed',
        data: data,
      });
    } catch (error) {
      return res.status(500).json({
        success: false,
        error: error.code,
        message: error,
        data: null,
      });
    }
  },
  update: async (req, res) => {
    try {
      const data = await model.user.update(
        {
          email: req.body.email,
          password: req.body.password,
          name: req.body.name,
          address: req.body.adress,
          role: req.body.role,
        },
        {
          where: {
            id: req.body.id,
          },
        }
      );

      return res.status(200).json({
        success: true,
        error: 0,
        message: 'data successfully listed',
        data: data,
      });
    } catch (error) {
      return res.status(500).json({
        success: false,
        error: error.code,
        message: error,
        data: null,
      });
    }
  },
  destroy: async (req, res) => {
    try {
      const data = await model.user.destroy({
        where: {
          id: req.body.id,
        },
      });

      return res.status(200).json({
        success: true,
        error: 0,
        message: 'data successfully deleted',
        data: data,
      });
    } catch (error) {
      return res.status(500).json({
        success: false,
        error: error.code,
        message: error,
        data: null,
      });
    }
  },
};
