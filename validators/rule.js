const { body } = require('express-validator');

const createUserRules = [
  body('name').notEmpty().withMessage('name is required'),
];

module.exports = {
  createUserRules,
};
