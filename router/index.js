const express = require('express');
const router = express.Router();
const userRouter = require('./user');
const storageRouter = require('./storage'); // memanggil folder storage.js
const archiveRouter = require('./archive');

router.get('/check', (req, res) => res.send('App Up'));
router.use('/user', userRouter);
router.use('/storage', storageRouter); // menggunakan route storage untuk proses crud
router.get('/archive', archiveRouter);

module.exports = router;
