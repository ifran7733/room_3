const express = require('express');
const router = express.Router();
const {
  list,
  create,
  update,
  destroy,
} = require('../controllers/storageController'); // memanggil semua fungsi yang ada di folder storageController

router.get('/list', list); // menampilkan semua data
router.post('/create', create); // manambahkan data baru
router.put('/update/:id', update); // update data
router.delete('/destroy/:id', destroy); // delete data

module.exports = router;
