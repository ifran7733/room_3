const express = require('express');
const router = express.Router();
const {
  list,
  create,
  update,
  destroy,
} = require('../controllers/userController');
const validate = require('../middleware/validate');
const { createUserRules } = require('../validators/rule');

router.get('/list', list);
router.post('/create', validate(createUserRules), create);
router.put('/update', update);
router.delete('/destroy', destroy);
module.exports = router;
