const express = require('express')
const router = express.Router()
const { list, create, update, destroy } = require('../controllers/archiveController')

router.get('/list', list)
router.post('/create', create)
router.put('/update', update)
router.delete('/destroy', destroy)


module.exports = router