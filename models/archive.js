'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class archive extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.storage)
    }
  }
  archive.init({
    name: {
      type: DataTypes.STRING,
      allowNull: false
      },
    description: {
      type: DataTypes.STRING,
      allowNull: false
      },
    storageId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'storages',
        key: 'id'
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    }
  }, {
    sequelize,
    modelName: 'archive',
    tableName: 'archives',
    timestamps: true
  });
  return archive;
};